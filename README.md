# Présentation

Cette application est une simple page d'accueil.

Elle est conçue pour **tester la bonne installation  d'un environnement avant déploiement** de l'application réelle, notamment les noms DNS, les reverses-proxy et leurs chainagse vers l'application cible et les certificats.

Elle est livrée sous forme d'un **conteneur** Docker et d'un fichier **Compose** car ciblant principalement l'offre d'hébergement Compose.

# Construction

Le fichier `Dockerfile` à la racine du projet permet de construire une image de l'application.  Depuis la racine du projet :

```shell
$ docker build -t welcome-page-app .
```

Pour démarrer l'application avec le titre "Mon application" :

```shell
$ docker run -d -e WELCOME_TITRE="Mon application" welcome-page-app
```

Un fichier `.gitlab-ci.yml` illustre la construction automatique depuis Gitlab.

# Configuration et déploiement en local

- Seul le titre peut être modifié, par la variable d'environnement `WELCOME_TITRE`.
- Le serveur Nginx écoute au sein du conteneur sur le port 80, à associer avec un port adéquat en externe

Le fichier `.env` fourni contient donc deux variables pour Docker-compose :

- `WELCOME_TITRE` : titre de la page d'accueil, nom de l'application par exemple
- `WELCOME_PORT` : port externe d'exposition du service Nginx

Pour lancer le service, renseigner le fichier `.env` puis :

```shell
$ docker-compose up -d
```
# Configuration et déploiement sur l'offre Eco Compose

Pour déployer sur l'offre Eco Compose :

- Ajoutez la variable `ECO_ACCESS_KEY` aux variables CI
- Copier le fichier `.env.exemple` et le renommer en `.env`
- Ajoutez l'étape de déploiement dont un exemple se trouve dans le fichier .gitlab-ci.yml

Plus d'information sur l'auto-déploiement Eco Compose dans la documentation SPS : https://portail-support.din.developpement-durable.gouv.fr/projects/demande-architecture-et-methode/documentation/Eco_compose

# Test en développement

Le fichier `docker-compose.dev.yml` construit une image docker à la volée à partir des sources de ce dépôt. 
Pour lancer un conteneur à l'aide de docker-compose, positionner la variable `WELCOME_TITRE` dans l'environnement (export, .env, ...) et lancer la commande suivante :

```shell
$ docker-compose -f docker-compose.dev.yml -d
```

# Personnalisation

Hormis le titre, cette application n'est pas personnalisable. Cependant sa simplicité fait qu'il est aisé de la cloner et la modifier.
