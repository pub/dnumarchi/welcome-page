# Image de base
FROM docker.io/nginx:1.27.2-alpine

ENV WELCOME_TITRE="Bienvenue,"
COPY nginx/default.conf.template /etc/nginx/templates/
COPY web /usr/share/nginx/html
